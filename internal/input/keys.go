/* Package input provide input for many devices

keys.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package input

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

var specialKeys = map[ebiten.Key]string{
	ebiten.KeyEnter:          "enter",
	ebiten.KeyNumpadEnter:    "enter",
	ebiten.KeyBackspace:      "back",
	ebiten.KeySpace:          "select",
	ebiten.KeyPageDown:       "hurrydown",
	ebiten.KeyPageUp:         "hurryup",
	ebiten.KeyHome:           "home",
	ebiten.KeyEnd:            "end",
	ebiten.KeyEscape:         "exit",
	ebiten.KeyNumpadAdd:      "volup",
	ebiten.KeyNumpadSubtract: "voldown",
	ebiten.KeyArrowDown:      "down",
	ebiten.KeyArrowLeft:      "up",
	ebiten.KeyArrowRight:     "down",
	ebiten.KeyArrowUp:        "up",
	ebiten.KeyF1:             "help",
	ebiten.KeyF5:             "run",
}

var charKeys = map[rune]string{
	'+': "volup",
	'-': "voldown",
	'h': "help",
	'H': "help",
	'i': "info",
	'I': "info",
	'v': "verify",
	'V': "verify",
	'r': "run",
	'R': "run",
	'c': "convert",
	'f': "colorscreen",
	'F': "colorscreen",
	's': "mute",
	'S': "mute",
}

func probeInputDevices() string {
	doIt := "" // do it Tomorrow
	var key ebiten.Key
	var action string
	var touche rune
	var runes []rune

	for key, action = range specialKeys {
		if inpututil.IsKeyJustPressed(key) {
			doIt = action
		}
	}

	runes = ebiten.AppendInputChars(runes[:0])

	if len(runes) > 0 {
		for touche, action = range charKeys {
			if touche == runes[0] {
				doIt = action
			}

		}
	}

	return doIt
}
