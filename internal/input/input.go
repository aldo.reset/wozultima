/* Package input provide input for many devices

input.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package input

import (
	"errors"

	"wozultima/internal/audios"
	"wozultima/internal/display"
	"wozultima/internal/workspace"
)

const (
	scale          = 1
	tps            = 120
	screenWidth    = 1410
	screenHeight   = 790
	dp             = 300
	normalFontSize = 16
	smallFontSize  = 11
)

var (
	delai          = int(0)
	ErrTerminated  = errors.New("Terminated")
	ModifWorkspace = bool(true)
	inputWait      = bool(false)
	nbLigneMax     = workspace.NbLignesEspace12 - 1
	cx             = int(0)
	cy             = int(0)

	err = error(nil)
)

func inputsProbe() string {
	var action = string("")

	action, cx, cy = infosPointersDevices()

	if action == "" {

		action = probeInputDevices()
	}

	return action

}

func InputManager() error {
	delai--
	action := inputsProbe()
	if delai < 1 {
		if !inputWait {
			ModifWorkspace = true
			display.MessageVisible = false

			switch action {
			case "down":

				addFocus()
			case "up":
				subFocus()

			case "home": //home
				display.RelativeFocus = 0
				display.Focus = 0

			case "end": // to the End
				for display.Focus < workspace.MaxEntries-1 {
					addFocus()
				}

			//moove selector (display.Focus)
			case "hurryup":
				for compteur := 0; compteur < 5; compteur++ {
					subFocus()
				}

			case "hurrydown":
				for compteur := 0; compteur < 5; compteur++ {
					addFocus()
				}

			// Next dir
			case "enter":
				if workspace.NextEspace(display.RelativeFocus) {
					display.Focus = 0
					display.RelativeFocus = 0

				}

			// Previous Dir
			case "back":
				if workspace.PreviousEspace() {
					display.Focus = 0
					display.RelativeFocus = 0

				}

			// screen color
			case "colorscreen":
				display.CurrentTextColor++
				if display.CurrentTextColor >= len(display.Couleurs) {
					display.CurrentTextColor = 0
				}

			// Woz Information
			case "info":

				if workspace.WorkDir.EntryIsWoz[display.Focus] {
					// Show Woz file informations
					display.MessageVisible = true
					display.MessageImage = true

					ModifWorkspace = false
					workspace.AfficheInfoWoz(display.Focus)

					inputWait = true
				}

			case "edit":
				// Edit Woz file
				if workspace.WorkDir.EntryIsWoz[display.Focus] {
				}
			case "verify":
				// Verify Woz file checksum if not exist
				if workspace.WorkDir.EntryIsWoz[display.Focus] {
					// Show Woz file informations
					display.MessageVisible = true
					display.MessageImage = true

					ModifWorkspace = false
					workspace.AfficheVerifyWoz(display.Focus)

					inputWait = true
				}

			case "mute":
				message := string("Sound is Mute")
				display.MessageVisible = true
				// Mute / Unmute Sound
				audios.MuteSound = !audios.MuteSound
				if audios.MuteSound {
					message = string("Sound is UnMute")
				}
				workspace.AfficheShortMessage("Mute", message)
				delai = 25
			case "volup":
				message := " "
				volume := int(audios.Volume)
				display.MessageVisible = true
				// Volume move
				volume += 8
				for compteur := 0; compteur < 128; compteur += 8 {
					if compteur <= volume {
						message = message + "☑"
					} else {
						message = message + "☐"
					}

				}
				if volume > 127 {
					volume = 128
					message = " ☑☑☑☑☑☑☑☑☑☑☑☑☑☑☑☑"
				}

				message = message + " "
				workspace.AfficheShortMessage("Volume Up", message)

				audios.Volume = float64(volume)
				delai = 25
			case "voldown":
				message := " "
				volume := int(audios.Volume)
				display.MessageVisible = true
				// Volume less
				volume -= 8
				for compteur := 0; compteur < 128; compteur += 8 {
					if compteur <= volume {
						message = message + "☑"
					} else {
						message = message + "☐"
					}

				}

				if volume < 8 {
					volume = 8
					message = " ☑☐☐☐☐☐☐☐☐☐☐☐☐☐☐☐"
				}

				message = message + " "
				workspace.AfficheShortMessage("Volume Down", message)

				audios.Volume = float64(volume)
				delai = 25
			case "exit":
				// exit program
				err = ErrTerminated
				display.MessageVisible = true

			case "help":
				// affiche Help
				display.MessageVisible = true
				display.MessageImage = false
				ModifWorkspace = false
				workspace.AfficheHelp()
				inputWait = true
			case "run":
				// Run WozFile with C600G

			case "click":
				var diffl int
				if cx > 30 && cx < workspace.NbCaractereEspace12*14 {
					if cy > normalFontSize*3 && cy < nbLigneMax*normalFontSize {

						nouveaufocus := int(cy/normalFontSize) - 3

						diffl = nouveaufocus - display.RelativeFocus

						if diffl < 0 {

							for diffl < 0 {
								subFocus()
								diffl++

							}

						} else {
							for diffl > 0 {
								addFocus()
								diffl--

							}

						}

					}
				}

			default:
				ModifWorkspace = false

			}
		} else {
			if action != "" {
				inputWait = false
				display.MessageVisible = false
				display.MessageImage = false
			}
		}
	}
	return err
}

func addFocus() {
	limit := workspace.MaxEntries - 1
	if display.RelativeFocus < workspace.NbLignesEspace12-5 && display.RelativeFocus < limit {
		display.RelativeFocus++
	}

	if display.Focus < limit {
		display.Focus++
	}

}

func subFocus() {
	if display.Focus > 0 {
		display.Focus--
	}

	if display.RelativeFocus > display.Focus {
		display.RelativeFocus--
	}

}
