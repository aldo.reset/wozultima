/* Package input provide input for many devices

pointers.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package input

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
)

func infosPointersDevices() (string, int, int) {
	// retourne la position du curseur touchpad ou souris et si il y a pointage

	px := int(0)
	py := int(0)
	click := "click" // TouchScreen like a click
	_, dy := ebiten.Wheel()
	for _, id := range ebiten.AppendTouchIDs(nil) {
		px, py = ebiten.TouchPosition(id)
	}
	if px == 0 && py == 0 {
		px, py = ebiten.CursorPosition()
		switch {
		case dy < 0:
			click = "down"
		case dy > 0:
			click = "up"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonFrontTopLeft):
			click = "up"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonFrontBottomLeft):
			click = "pageup"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonLeftLeft):
			click = "up"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonLeftTop):
			click = "up"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonLeftRight):
			click = "down"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonLeftBottom):
			click = "down"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonFrontBottomRight):
			click = "pagedown"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonFrontTopRight):
			click = "down"
		case inpututil.IsStandardGamepadButtonJustPressed(0, ebiten.StandardGamepadButtonRightBottom):
			click = "click"
		case inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft):
			click = "click"
		case inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonMiddle):
			click = "click"
		case inpututil.IsMouseButtonJustPressed(ebiten.MouseButton4):
			click = "click"
		case inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonRight):
			click = "click"
		default:
			click = ""
		}

	}

	return click, px, py
}
