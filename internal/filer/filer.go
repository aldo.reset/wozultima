/* Package filer provide files read write and analyse especially for Woz Format

filer/filer.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package filer

import (
	"bufio"
	"io"
	"log/slog"
	"os"
	"path/filepath"
	"wozultima/internal/woztools"
)

type sortType string

const (
	sortName     sortType = "Name[^]"
	sortNameRev  sortType = "Name[$]"
	sortSize     sortType = "Size[^]"
	sortSizeRev  sortType = "Size[$]"
	sortMtime    sortType = "Time[^]"
	sortMtimeRev sortType = "Time[$]"
)

type Dir struct {
	Name            string
	NbDirs          int
	NbFiles         int
	GlobalSize      int64
	EntryName       []string
	EntryIsdir      []bool
	EntryIsWoz      []bool
	EntryCrc32Woz   []uint32
	EntryIsCrcOkWoz []bool
	EntryWozVersion []uint8
	EntrySize       []int64
	EntryDate       []string
	Sort            sortType
}

func check(e error) {
	if e != nil {
		if e != io.EOF {
			slog.Error("WozUltima: %v", e)
		}
	}
}

var HomeDirName string
var err error

func NewDir(nameDir string) Dir {
	var workDir Dir
	HomeDirName, err = os.UserHomeDir()
	check(err)

	workDirName := HomeDirName

	if nameDir != "" {

		workDirName = nameDir
	}
	workDir = ReadDir(workDirName)
	if workDir.NbDirs+workDir.NbFiles == 0 {
		workDir = ReadDir(HomeDirName)
	}

	check(os.Chdir(workDir.Name))
	return workDir

}

func ReadDir(parts ...string) Dir {
	var d Dir
	entryDirName := filepath.Join(parts...)

	entries, err := os.ReadDir(entryDirName)
	check(err)
	d.Name = entryDirName
	d.GlobalSize = 0
	d.NbDirs = 0
	d.NbFiles = 0
	d.Sort = sortName

	compteur := 0
	for _, entry := range entries {

		info, err := entry.Info()
		check(err)
		name := info.Name()
		mode := info.Mode()
		size := info.Size()
		isDir := info.IsDir()

		if mode&os.ModeSymlink == 0 {
			if selectEntry(entryDirName, name) {
				d.EntryName = append(d.EntryName, name)

				d.EntryDate = append(d.EntryDate, info.ModTime().Format("2006-01-02 15:04"))
				d.EntrySize = append(d.EntrySize, 0)
				d.EntryIsdir = append(d.EntryIsdir, isDir)
				d.EntryIsWoz = append(d.EntryIsWoz, false)
				d.EntryWozVersion = append(d.EntryWozVersion, 0)
				d.EntryIsCrcOkWoz = append(d.EntryIsCrcOkWoz, false)
				d.EntryCrc32Woz = append(d.EntryCrc32Woz, 0)
				if isDir {
					d.NbDirs++
				} else {
					d.GlobalSize += size
					d.NbFiles++
					d.EntrySize[compteur] = size
					d.EntryIsWoz[compteur] = woztools.IsWozFile(entryDirName, d.EntryName[compteur])
					if d.EntryIsWoz[compteur] {
						d.EntryWozVersion[compteur], d.EntryIsCrcOkWoz[compteur], d.EntryCrc32Woz[compteur] = woztools.CheckWoz(entryDirName, d.EntryName[compteur])

					}
				}

				compteur++
			}

		}
	}
	return d
}

func ReadEchantillonFile(fileName string) []byte {
	file, err := os.Open(fileName)
	check(err)
	defer file.Close()

	reader := bufio.NewReader(file)
	buf := make([]byte, 1152)

	_, err = reader.Read(buf)
	check(err)

	return buf
}

func ChangePreviousDir() Dir {

	check(os.Chdir(".."))
	workDirName, err := os.Getwd()
	check(err)

	return ReadDir(workDirName)

}

func ChangeNextDir(parts ...string) (Dir, bool) {
	isNext := false
	workDirName := filepath.Join(parts...)
	workDir := ReadDir(workDirName)
	if workDir.NbDirs+workDir.NbFiles != 0 {

		check(os.Chdir(workDir.Name))
		isNext = true
	}

	return workDir, isNext

}
