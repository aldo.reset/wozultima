//go:build !windows

/* Package filer provide files read write and analyse especially for Woz Format

filer/filer.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package filer

import (
	"os"
	"path/filepath"
)

func selectEntry(dirName string, fileName string) bool {

	isSelect := true

	path := filepath.Join(dirName, fileName)

	file, err := os.Open(path)
	if err != nil {
		isSelect = false
	} else {
		defer file.Close()
	}

	if fileName[0] == '.' || fileName[len(fileName)-1] == '~' {
		isSelect = false
	}

	return isSelect
}
