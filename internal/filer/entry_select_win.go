//go:build windows

/* Package filer provide files read write and analyse especially for Woz Format

filer/filer.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package filer

import (
	"os"
	"path/filepath"
	"syscall"
)

func selectEntry(dirName string, fileName string) bool {
	isSelect := bool(true)

	path := filepath.Join(dirName, fileName)

	pointer, err := syscall.UTF16PtrFromString(path)
	check(err)
	attributes, err := syscall.GetFileAttributes(pointer)
	check(err)
	isSelect = attributes&syscall.FILE_ATTRIBUTE_HIDDEN == 0

	file, err := os.Open(path)
	if err != nil {
		isSelect = false
	} else {
		defer file.Close()
	}

	if fileName[0] == '.' || fileName[len(fileName)-1] == '~' {
		isSelect = false
	}

	return isSelect

}
