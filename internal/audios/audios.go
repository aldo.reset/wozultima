/* Package audios provide WozUltima sounds

audios/audios.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package audios

import (
	"bytes"
	"log/slog"
	"wozultima/internal/assets/sounds"

	"github.com/hajimehoshi/ebiten/v2/audio"
	"github.com/hajimehoshi/ebiten/v2/audio/wav"
)

const (
	sampleRate = 44100
)

var (
	audioContext *audio.Context
	audioPlayer  *audio.Player
	MuteSound    = bool(false)
	Volume       = float64(128)
)

func check(e error) {
	if e != nil {
		slog.Error("WozUltima: %v", e)
	}
}

func InitAudio() {
	audioContext = audio.NewContext(sampleRate)

}

func Beep() {
	if MuteSound {
		beep, err := wav.DecodeWithoutResampling(bytes.NewReader(sounds.AppleIIBeep))
		check(err)
		audioPlayer, err = audioContext.NewPlayer(beep)
		check(err)
		audioPlayer.Rewind()
		audioPlayer.SetVolume(Volume / 128)

		audioPlayer.Play()
	}
}

func BootDrive() {
	if MuteSound {
		boot, err := wav.DecodeWithoutResampling(bytes.NewReader(sounds.BootDriveSound))
		check(err)
		audioPlayer, err = audioContext.NewPlayer(boot)
		check(err)
		audioPlayer.Rewind()
		audioPlayer.SetVolume(Volume / 128)
		audioPlayer.Play()
	}

}
