/* Package fonts provide WozUltima fonts

fonts/embed.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package fonts

import (
	_ "embed"
)

var (

	//go:embed WozUltima_Font.otf
	WozUltimaFont []byte

	//go:embed WozUltima_Neg_Font.otf
	WozUltimaNegFont []byte
)
