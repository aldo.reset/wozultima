/* Package images provide WozUltima images

images/embed.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package images

import (
	_ "embed"
)

var (

	//go:embed Generic-WozFileCover.png
	WozGenericCoverImage []byte
)
