/* Package display provide workspace displaying

display.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package display

import (
	"bytes"
	"image"

	"image/color"
	_ "image/png"
	"log/slog"
	"wozultima/internal/assets/fonts"
	"wozultima/internal/assets/images"
	"wozultima/internal/workspace"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/nfnt/resize"

	"github.com/hajimehoshi/ebiten/v2/text/v2"
	"github.com/hajimehoshi/ebiten/v2/vector"
)

const (
	scale          = 1
	tps            = 120
	screenWidth    = 1410
	screenHeight   = 790
	dp             = 300
	normalFontSize = 16
	smallFontSize  = 11
)

const (
	nbLignesTotal    = workspace.NbLignesTotal
	nbLignesEspace12 = workspace.NbLignesEspace12
	nbLignesEspace3  = workspace.NbLignesEspace3
)

var (
	fontSource       *text.GoTextFaceSource
	fontSourceNeg    *text.GoTextFaceSource
	Couleurs         = [...]color.RGBA{{0x00, 0xe7, 0x00, 0xff}, {0xff, 0xbd, 0x04, 0xff}, {0xff, 0xff, 0xff, 0xff}} // vert, ambre , blanc
	MessageVisible   = bool(false)
	CurrentTextColor = int(0)

	Focus         = int(0)
	RelativeFocus = int(0)

	MessageImage    = bool(false)
	MessageImagePng *ebiten.Image
)

func check(e error) {
	if e != nil {
		slog.Error("WozUltima: %v", e)
	}
}

func InitDisplay() {
	source, err := text.NewGoTextFaceSource(bytes.NewReader(fonts.WozUltimaFont))
	check(err)

	fontSource = source

	source, err = text.NewGoTextFaceSource(bytes.NewReader(fonts.WozUltimaNegFont))
	check(err)

	fontSourceNeg = source

}

func DisplayWorkspace(screen *ebiten.Image) {
	couleurFonte := Couleurs[CurrentTextColor]

	lignes := int(0)
	x := float64(20)
	y := float64(normalFontSize)

	// espace 1
	op := &text.DrawOptions{}
	op.GeoM.Translate(x, y)
	op.ColorScale.ScaleWithColor(couleurFonte)
	texte := workspace.LigneExtract(0, lignes)
	text.Draw(screen, texte, &text.GoTextFace{
		Source: fontSource,
		Size:   normalFontSize,
	}, op)

	lignes++

	for lignes < nbLignesEspace12 {
		y += normalFontSize
		op = &text.DrawOptions{}
		op.GeoM.Translate(x, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		texte := workspace.LigneExtract(0, lignes)

		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		if lignes == RelativeFocus+2 {
			texte = workspace.LigneExtract(1, lignes)
			op = &text.DrawOptions{}
			op.GeoM.Translate(x+normalFontSize, y)
			op.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSourceNeg,
				Size:   normalFontSize,
			}, op)

		} else {
			texte = workspace.LigneExtract(1, lignes)
			op = &text.DrawOptions{}
			op.GeoM.Translate(x+normalFontSize, y)
			op.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSource,
				Size:   normalFontSize,
			}, op)
		}

		lignes++
	}

	// espace 2
	lignes = 0
	x = 708
	y = normalFontSize

	op = &text.DrawOptions{}
	op.GeoM.Translate(x, y)
	op.ColorScale.ScaleWithColor(couleurFonte)

	texte = workspace.LigneExtract(2, lignes)
	text.Draw(screen, texte, &text.GoTextFace{
		Source: fontSource,
		Size:   normalFontSize,
	}, op)

	lignes++

	for lignes < nbLignesEspace12 {

		y += normalFontSize
		texte = workspace.LigneExtract(2, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		texte = workspace.LigneExtract(3, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x+normalFontSize, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   smallFontSize,
		}, op)

		lignes++
	}

	// espace 3
	x = float64(20)
	y += normalFontSize
	lignes = 0

	op = &text.DrawOptions{}
	op.GeoM.Translate(x, y)
	op.ColorScale.ScaleWithColor(couleurFonte)

	texte = workspace.LigneExtract(4, lignes)
	text.Draw(screen, texte, &text.GoTextFace{
		Source: fontSource,
		Size:   normalFontSize,
	}, op)
	lignes++

	for lignes < nbLignesEspace3 {

		y += normalFontSize
		texte = workspace.LigneExtract(4, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		texte = workspace.LigneExtract(5, lignes)
		op = &text.DrawOptions{}
		op.GeoM.Translate(x+normalFontSize, y)
		op.ColorScale.ScaleWithColor(couleurFonte)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, op)

		lignes++
	}
	//affiche second windows
	if MessageVisible {

		sw, sh := screen.Bounds().Dx(), screen.Bounds().Dy()
		iw, ih := workspace.NbCaracteresEspaceMessage*14, workspace.NbLignesEspaceMessage*normalFontSize
		x = float64(sw)/2 - float64(iw)/2
		y = float64(sh)/2 - float64(ih)/2
		vector.DrawFilledRect(screen, float32(x), float32(y), float32(iw), float32(ih), color.Black, false)
		if MessageImage {
			uneImage, _, err := image.Decode(bytes.NewReader(images.WozGenericCoverImage))
			check(err)
			uneImage = resize.Resize(192, 260, uneImage, resize.Lanczos3)
			MessageImagePng = ebiten.NewImageFromImage(uneImage)
			mx := (x + float64(iw)) - float64(MessageImagePng.Bounds().Dx())
			op := &ebiten.DrawImageOptions{}
			op.GeoM.Translate(mx-12, y+12)
			screen.DrawImage(MessageImagePng, op)
		}
		// espace help

		lignes = 0

		y += 5
		opt := &text.DrawOptions{}
		opt.GeoM.Translate(x, y)
		opt.ColorScale.ScaleWithColor(couleurFonte)

		texte = workspace.LigneExtract(6, lignes)
		text.Draw(screen, texte, &text.GoTextFace{
			Source: fontSource,
			Size:   normalFontSize,
		}, opt)
		lignes++

		for lignes < workspace.NbLignesEspaceMessage {

			y += normalFontSize
			texte = workspace.LigneExtract(6, lignes)
			opt = &text.DrawOptions{}
			opt.GeoM.Translate(x, y)
			opt.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSource,
				Size:   normalFontSize,
			}, opt)

			texte = workspace.LigneExtract(7, lignes)
			opt = &text.DrawOptions{}
			opt.GeoM.Translate(x+normalFontSize, y+2)
			opt.ColorScale.ScaleWithColor(couleurFonte)
			text.Draw(screen, texte, &text.GoTextFace{
				Source: fontSource,
				Size:   normalFontSize,
			}, opt)

			lignes++
		}

	}

}
