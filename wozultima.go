/* Package main provide start environment for many devices

wozultima.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License

*/

package main

import (
	_ "image/png"

	"log/slog"

	"wozultima/config"
	"wozultima/internal/audios"
	"wozultima/internal/display"
	"wozultima/internal/input"
	"wozultima/internal/workspace"

	"github.com/hajimehoshi/ebiten/v2"
)

const (
	scale          = 1
	tps            = 120
	screenWidth    = 1410
	screenHeight   = 790
	dp             = 300
	normalFontSize = 16
	smallFontSize  = 11
)

type Game struct {
	config *config.Config
}

func check(e error) {
	if e != nil {
		slog.Error("WozUltima: %v", e)
	}
}

func main() {

	audios.InitAudio()

	conf, err := config.LoadConfig()
	check(err)

	game := &Game{
		config: conf,
	}

	display.CurrentTextColor = game.config.LastTextColor
	workspace.WorkingDirPath = game.config.LastWorkingDirPath
	audios.MuteSound = game.config.UnMuteSound
	audios.Volume = game.config.SoundVolume
	audios.Beep()

	display.InitDisplay()
	// polices de caractère

	workspace.WorkingDirPath = workspace.CreerEspace(workspace.WorkingDirPath)
	game.config.LastWorkingDirPath = workspace.WorkingDirPath

	ebiten.SetWindowSize(screenWidth*scale, screenHeight*scale)
	ebiten.SetWindowTitle("WozUltima")
	ebiten.SetWindowResizingMode(ebiten.WindowResizingModeDisabled)
	ebiten.SetTPS(tps)
	if err := ebiten.RunGameWithOptions(game, &ebiten.RunGameOptions{ScreenTransparent: false}); err != nil {
		if err != input.ErrTerminated {
			// Irregular termination
			slog.Error("Damned ! %v\n", err)
		}

	}
	check(conf.SaveToFile())

}

func (g *Game) Layout(outsideWidth, outsideHeight int) (int, int) {
	return screenWidth, screenHeight
}

func (g *Game) Draw(screen *ebiten.Image) {

	display.DisplayWorkspace(screen)

}

func (g *Game) Update() error {
	err := error(nil)

	err = input.InputManager()
	if input.ModifWorkspace {
		workspace.AfficheEspace(display.Focus)
		g.config.LastTextColor = display.CurrentTextColor
		g.config.LastWorkingDirPath = workspace.WorkDir.Name
		g.config.UnMuteSound = audios.MuteSound
		g.config.SoundVolume = audios.Volume
	}

	return err
}
