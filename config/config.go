/* Package config provide start environment

config/config.go

WozUltima a swiss army knife for managing, manipulating, analysing and much more Woz format files
(C) 2024 Aldo Reset from CCB (https://cleancrackband.org) - GNU Affero General Public License
*/

package config

import (
	"encoding/json"
	"errors"
	"log/slog"
	"os"
	"path/filepath"
)

type Config struct {
	LastTextColor      int     `json:"lastTextColor"`
	LastWorkingDirPath string  `json:"workingDirPath"`
	UnMuteSound        bool    `json:"unmuteSound"`
	SoundVolume        float64 `json:"soundVolume"`
}

const (
	defaultConfigDir  = "WozUtlima"
	defaultConfigFile = "config.json"
)

var config_path string // not public = not exported

var StartDir string
var err error
var file *os.File

func check(e error) {
	if e != nil {
		slog.Error("WozUltima: %v", e)
	}
}

func LoadConfig() (*Config, error) {
	var conf Config
	// initialisation
	StartDir, err = os.Getwd()
	check(err)
	userConfigDir, err := os.UserConfigDir()
	check(err)
	userHomeDir, err := os.UserHomeDir()
	check(err)
	config_path = filepath.Join(userConfigDir, defaultConfigDir, defaultConfigFile)
	if _, err = os.Lstat(config_path); err == nil {
		// path/to/whatever exists
		if file, err = os.Open(config_path); err == nil {
			defer file.Close()

			err = json.NewDecoder(file).Decode(&conf)
		}

	} else if errors.Is(err, os.ErrNotExist) {
		// path/to/whatever does *not* exist
		slog.Info("config: create default:", "path", config_path)
		conf.LastTextColor = 0
		conf.LastWorkingDirPath = userHomeDir
		conf.UnMuteSound = true
		conf.SoundVolume = 128
		if err = os.MkdirAll(filepath.Dir(config_path), 0770); err == nil {
			err = conf.SaveToFile()
		}

	}

	return &conf, err
}

func (c *Config) SaveToFile() error {

	file, err = os.Create(config_path)
	if err == nil {
		defer file.Close()
		err = json.NewEncoder(file).Encode(c)

	}

	return err
}
